require 'rest_client'

class GasPrices

  BASE_URL = 'http://gas.arjanfrans.nl'
  QUERY_STRING = '/fuel_types/'

  URL = BASE_URL + QUERY_STRING

  def initialize(*args)
    @types = getTypes
  end

  def isType(description)
    !getType(description).nil?
  end

  def getPrice(description)
    if isType(description)
      response = RestClient.get URL + description
      (JSON.parse(response.to_str)['fuel_price']['price']).to_f if response.code == 200
    end
  end

  private
    def getType(description)
      @types.each do |type|
        type if type['description'] == description
      end
    end

    def getTypes
      response = RestClient.get URL
      JSON.parse(response.to_str) if response.code == 200
    end

end