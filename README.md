# GasPrices

TODO: Write a gem description

## Installation

Add this line to your application's Gemfile:

```ruby
gem 'gas_prices'
```

And then execute:

    $ bundle

Or install it yourself as:

    $ gem install gas_prices

## Usage

Check if type exists.
```ruby
gas_prices = GasPrices.new
response = gas_prices.isType('lng')
```

Get price of the type.
```ruby
gas_prices = GasPrices.new
response = gas_prices.getPrice('lng')
```


## Contributing

1. Fork it ( https://github.com/[my-github-username]/gas_prices/fork )
2. Create your feature branch (`git checkout -b my-new-feature`)
3. Commit your changes (`git commit -am 'Add some feature'`)
4. Push to the branch (`git push origin my-new-feature`)
5. Create a new Pull Request
